#!/bin/bash
#cd "${0%/*}"
cd "$HOME/scripts/crunchypv/"
source ./playScripts.sh

####INICIO#####

if [[ $# -eq 0 ]]; then
	./help.sh
	exit 0
fi	

FORMAT=0;
PLAYLIST=0;
USER=0;	

while getopts n:e:f:i:lhu option
do
	case "${option}"
		in
		n) NAME=${OPTARG};;
		e) EPISODES=${OPTARG};;
		f) FORMAT=${OPTARG};;
		l) LANGUAGE=${OPTARG};;
		u) USER=1;;
		i) IGNORE=${OPTARG};;
		h) ./help.sh && exit 0;;
	esac
done

[[ -z ${NAME} || -z ${EPISODES} ]] && ./help.sh

[[ $EPISODES =~ [a-z]  ]] && playmovie || createPlaylist ; [[ $EPISODES =~ [0-9]+,,[0-9]+ ]] && playBatch || playOne
