#!/bin/bash

#Scrap html del show y hacer una lista con los links de los episodios
createPlaylist(){
	echo "Creating links list list..."
	
	curl -A "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0" -sL https://www.crunchyroll.com/${NAME} | awk -F '/|"\ |\t' '{ if($1 ~ /href/ && $4 ~ /-[0-9]+-/) print $4}' > list.txt
	if [ ! -s list.txt ]; then
		echo "curl no anduvo, testeando con ruby"
		echo "NAME: $NAME"
		echo "IGNORE: $IGNORE"
		ruby lista.rb $NAME $IGNORE
		if [ ! -s list.txt ]; then
			echo "ERROR, either the show isn't available in your country or it doesn't exist, make the NAME is the same as in http://www.crunchyroll.com/NAME/something"
			exit 1
		fi
	else
		echo "done"
	fi
}

#busca el formato y  reproduce cada episodio
play(){
	echo ""
	echo ""
	echo "PLAY"
	echo ""
	echo ""
	echo "$LINK"
	echo "retrieving format..."
	if [[ "$USER" == 0 ]]
	then
		echo ""
		echo "NO USER"
		echo ""
		echo 'YTDL="--ytdl-raw-options=format=$(youtube-dl -F' $LINK 
		if [[ "$FORMAT" == 0 ]]
		then
			YTDL=""
		else
			YTDL="--ytdl-raw-options=format=$(youtube-dl -F $LINK  |   awk -v f="$FORMAT" '($3 ~ f && $1 ~/jaJP-[0-9]/) {print $1;exit}')"
		fi
	else
		USERNAME=$(awk '/u:/ {print $2 }' pass.txt)
		PASSWORD=$(awk '/p:/ {print $2 }' pass.txt)
		echo 'YTDL="--ytdl-raw-options=username=$USERNAME,password=$PASSWORD,format=$(youtube-dl --username $USERNAME --password $PASSWORD -F $LINK  |   awk -v f= $FORMAT  ($3 ~ f && $1 ~/jaJP-[0-9]/) {print $1} )"'
		YTDL="--ytdl-raw-options=username=$USERNAME,password=$PASSWORD,format=$(youtube-dl --username $USERNAME --password $PASSWORD -F $LINK  |   awk -v f="$FORMAT" '($3 ~ f && $1 ~/jaJP-[0-9]/) {print $1}')"
	fi
	echo "done"

	echo ""
	echo "mpv $YTDL $LINK"
	echo ""
	mpv "$YTDL" "$LINK" 
}


playMovie(){
	echo "entro a playmovie"
		MOVIE=$(curl -A "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0" -sL https://www.crunchyroll.com/${NAME} | awk -v var=$EPISODES -F '/|"\ |\t' '{ if($1 ~ /href/ && $4 ~ var) print $4}')
		LINK="https://www.crunchyroll.com/$NAME/$MOVIE"

	play
}

#crea el link de cada episodio cuando se reproduce a,b,c
playOne(){
	echo ""
	echo ""
	echo "PLAYONE"
	echo ""
	echo ""

	IFS=,;
	epi=($EPISODES);

	for (( i=0; i<${#epi[@]};i++));do
		
		ep_num="${epi[$i]}"
		#LINK="https://www.crunchyroll.com/$NAME/"$(cat ./list.txt | awk -v EP_NUM="-${ep_num}-" -F'/|" |\t' '{ if($0 ~ EP_NUM ) print}')
		LINK=$(cat ./list.txt | awk -v EP_NUM="episode-${ep_num}-" -F'/|" |\t' '{ if($0 ~ EP_NUM ) print}')
		echo "LINK: $LINK"
		play
	
	done
}

#crea el link de cada episodio cuando se reproduce desde a hasta x
playBatch(){
	echo ""
	echo ""
	echo "PLAYBATCH"
	echo ""
	echo ""
   	IFS=,,;
	a=($EPISODES); 
	inicio=${a[0]}; 
	fin=${a[2]}; 

	for (( i="$inicio"; i <= "$fin"; i++ ));do
		#EP_LINK=$(cat ./list.txt | awk -v EP_NUM="-${i}-" -F'/|"\ |\t' '{ if($1 ~ EP_NUM ) print}')
		LINK=$(cat ./list.txt | awk -v EP_NUM="-${i}-" -F'/|" |\t' '{ if($0 ~ EP_NUM ) print}')
		#LINK="https://www.crunchyroll.com/$NAME/$EP_LINK"
		play		
		done
}
