require 'mechanize'
agent=Mechanize.new
pag=agent.get("http://www.crunchyroll.com/"+ARGV[0])
puts(ARGV[1])
links=[]

pag.links_with(href:/episode/).each do |link|
  if ARGV[1]
    if  ! link.href.match(/#{ARGV[1]}/)
      links.append(link.href)
    end
  else
    links.append(link.href)
  end
end

File.write("list.txt","")

##el primer link de la lista va a ser el ultimo capitulo de la serie
episodios=links[0].scan(/episode-\d+/).to_s.scan(/\d+/)[0].to_i

#si hay mas links que capitulos (por ejemplo dubs y audio original)
#los primeros links van a ser el audio original, se descartan los demas

if (links.length-1 > episodios)
  aux=[]
  for i in 0..episodios-1
    aux.push(links[i])
  end
  links = aux
else
end

#la lista queda del ultimo al primer episodio
#se la recorre al reves para crear el archivo
links.reverse.each do |link|
  File.write("list.txt","https://www.crunchyroll.com"+link+"\n",mode:"a")
end
